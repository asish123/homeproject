import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AuthorsService {

  typesOfAuthors:any =  [{id: 1 ,author:'Lewis Carrol'}, {id:2 , author: 'Leo Tolstoy'},{id:3, author: 'Thomas Mann'}, {id:4, author:'Angie Fox'}, {id:5, author:'April White'}];
  i:number = this.typesOfAuthors.length;

  getAuthors(){
   const authorsObservable = new Observable(
      observer =>{
        setInterval (
          ()=> observer.next(this.typesOfAuthors),4000
        )
      }
    )
    return authorsObservable;
  }

  authors(typesOfAuthors: any): void {
    throw new Error("Method not implemented.");
  }

 addAuthors(authornew:string){
    this.i=this.i+1;
    this.typesOfAuthors.push({id:this.i,author:authornew})

  }

  constructor() { }
}
