import { Component, OnInit } from '@angular/core';
import { Router,UrlTree } from '@angular/router';

@Component({
  selector: 'app-editauthor',
  templateUrl: './editauthor.component.html',
  styleUrls: ['./editauthor.component.css']
})
export class EditauthorComponent implements OnInit {
  author:string;
  id:number;
  onSubmit(){
       console.log("yada yada");
         this.router.navigate(['/authors',this.id ,this.author]);
       }
     
       constructor(private router: Router) {
     
        }
     
       ngOnInit() {
       
         let url = this.router.url
         let urlArr = url.split('/')
        this.author = decodeURI(urlArr[3])
        this.id = parseInt(urlArr[2])
     
      }
     
     }