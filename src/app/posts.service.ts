import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Post } from './interface/post';

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  apiurl = "https://jsonplaceholder.typicode.com/posts "

  constructor(private _http: HttpClient) { }
  
  
  getPost() {
    return this._http.get<Post[]>(this.apiurl);
  }
}
