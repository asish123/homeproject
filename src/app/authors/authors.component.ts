import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthorsService } from './../authors.service';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {
  //*typesOfAuthors: object[] = [{id: 1 ,author:'Lewis Carrol'}, {id:2 , author: 'Leo Tolstoy'},{id:3, author: 'Thomas Mann'}, {id:4, author:'Angie Fox'}, {id:5, author:'April White'}]; 
 
  constructor(private route: ActivatedRoute,private authorservice:AuthorsService) { }

  author;
  id;
  newauthor:string;
  authors:any;
  authors$:Observable<any>;

  add(){
    this.authorservice.addAuthors(this.newauthor);
      }

  ngOnInit() {
    var that = this;
    this.authors$ = this.authorservice.getAuthors();
     this.author = this.route.snapshot.params.author;
     this.id = this.route.snapshot.params.id;

     this.authors$.forEach(function(obj){
          console.log(that.id)

           if(obj['id'] == that.id){
             obj['author'] = that.author;
           }
         })

      }
  }
