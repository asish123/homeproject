import { Component, OnInit } from '@angular/core';
import { Post } from './../interface/post';
import { PostsService } from './../posts.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  panelOpenState = false; 

  Posts$: Post[];


  constructor(private postsrvice: PostsService) { }

  ngOnInit() {
    return this.postsrvice.getPost().subscribe(data =>this.Posts$ = data );
  }

}
