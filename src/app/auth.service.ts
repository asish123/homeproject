import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { User } from './interfaces/user';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';



@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user : Observable< User | null>

  constructor(public afAuth: AngularFireAuth, public router:Router) { 
    this.user = this.afAuth.authState;
  }
  
  signup(email:string,password:string){
    this.afAuth
    .auth
    .createUserWithEmailAndPassword(email,password)
    .then(res => console.log('Succesful Signup',res))
  }

  Logout(){
    this.afAuth.auth.signOut();  
  }
  login(email:string, password:string){
    this.afAuth
        .auth.signInWithEmailAndPassword(email,password)
        .then(
           res =>  
            {
              console.log('Succesful Login',res);
              this.router.navigate(['/books']);
            }     
        )
  }

}
