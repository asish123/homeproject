// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBDcEmhAYEshmi_HnlA8x3NJaDthGIE2fk",
    authDomain: "asisapp-a3e9d.firebaseapp.com",
    databaseURL: "https://asisapp-a3e9d.firebaseio.com",
    projectId: "asisapp-a3e9d",
    storageBucket: "asisapp-a3e9d.appspot.com",
    messagingSenderId: "78661808753",
    appId: "1:78661808753:web:18cb7e5165f873ee59a1d0",
    measurementId: "G-PNRFKXQS9Z"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
